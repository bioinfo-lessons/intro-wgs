export WD=~/intro-wgs-long
mkdir -p $WD
cd $WD
mkdir -p data res out log

conda config --add channels defaults
conda config --add channels bioconda
conda config --add channels conda-forge

mamba create -y -n intro-wgs-long flye assembly-stats bwa samtools wget

wget -P data https://bioinformatics.cnio.es/data/courses/intro-wgs/long_reads.fastq.gz
