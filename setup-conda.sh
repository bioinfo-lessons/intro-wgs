fname="Miniforge3-$(uname)-$(uname -m).sh"

if command -v curl &> /dev/null
then
    curl -L -O https://github.com/conda-forge/miniforge/releases/latest/download/$fname
elif command -v wget &> /dev/null
then
    wget https://github.com/conda-forge/miniforge/releases/latest/download/$fname
else
    echo
    echo "ERROR: Neither curl nor wget were available. Please install one of them to proceed."
    echo
    exit 1
fi

bash $fname -b

export PATH="$HOME/miniforge3/bin:$PATH"
conda init
conda config --add channels nodefaults
conda config --add channels bioconda
conda config --add channels conda-forge
conda config --set channel_priority strict
conda config --set auto_activate_base false

rm $fname

echo
echo "Setup completed. Please close and reopen your terminal for changes to take effect."
echo
