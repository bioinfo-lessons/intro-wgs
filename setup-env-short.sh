export WD=~/intro-wgs
mkdir -p $WD
cd $WD
mkdir -p data res out log

conda config --add channels nodefaults
conda config --add channels bioconda
conda config --add channels conda-forge

mamba create -y -n intro-wgs abyss bwa igv mummer samtools seqtk gnuplot wget

wget -P data/ https://bioinformatics.cnio.es/data/courses/intro-wgs/30CJCAAXX_4_1.fq.gz
wget -P data/ https://bioinformatics.cnio.es/data/courses/intro-wgs/30CJCAAXX_4_2.fq.gz

mkdir -p res/genome
wget -P res/genome/ http://hgdownload.cse.ucsc.edu/goldenpath/hg38/chromosomes/chr3.fa.gz
gunzip res/genome/chr3.fa.gz
